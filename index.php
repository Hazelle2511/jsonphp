<?php
$datas = json_decode(file_get_contents('cs_figures.json'));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP JSon/CSV</title>
    <!-- <script src="Y/src/theme.less"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css" integrity="sha512-u5E5vOHVsB0blcZniDNfaRkTfqdgjxIgLX17YlG0vjQyos7Z7b1BXLuhHZbF2JOVRrfNNd8P6v7fw87JaFw/Rg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="ui link cards sixteen wide column three wide column">

<?php foreach($datas as $data): ?>
 

  <div class="card sixteen wide column three wide column">
 
    <div class="image">
  
      <img src="<?php echo $data->picture; ?>">
    </div>
    <div class="content">
      <div class="header"><?php echo $data->name; ?></div>
      <!-- <div class="meta">
        <a>Friends</a>
      </div> -->
      <div class="meta">
      <?php echo $data->title; ?>
      </div>
      <!-- </div> -->
    </div>
    <div class="description">
    <?php echo $data->role; ?></div>
    <!-- </div> -->
    <div class="extra content">
    <div class="right floated">
    
    <?php echo $data->birthyear; ?></div>
    </div>
    <!-- </div> -->
  </div>

  <?php endforeach; ?>
</div>

</body>
</html>
   




    
